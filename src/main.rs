use std::fs::File;

use clap::{Arg, ArgAction, Command};
use csv::Reader;

const ARG_ID_SHOW_HEADERS: &'static str = "show headers";
const ARG_ID_SELECT_HEADER: &'static str = "header";
const ARG_ID_CSV_FILE: &'static str = "CSV file";

enum Header<'a> {
    Index(usize),
    Name(&'a String),
}

impl<'a> From<&'a String> for Header<'a> {
    fn from(value: &'a String) -> Self {
        match value.parse::<usize>() {
            Ok(i) => Header::Index(i),
            Err(_) => Header::Name(value),
        }
    }
}

fn main() {
    let matches = Command::new("CSV Utility")
        .version(env!("CARGO_PKG_VERSION"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .arg(
            Arg::new(ARG_ID_SHOW_HEADERS)
                .action(ArgAction::SetTrue)
                .long("show-headers")
                .short('L')
                .help("Print the headers (first line) then exit"),
        )
        .arg(
            Arg::new(ARG_ID_SELECT_HEADER)
                .action(ArgAction::Append)
                .long("header")
                .short('H')
                .conflicts_with(ARG_ID_SHOW_HEADERS)
                .help("Select the header by index or name for printing"),
        )
        .arg(
            Arg::new(ARG_ID_CSV_FILE)
                .action(ArgAction::Set)
                .index(1)
                .help("The CSV file to parse"),
        )
        .get_matches();

    let mut reader: Reader<Box<dyn std::io::Read>> =
        if let Some(filename) = matches.get_one::<String>(ARG_ID_CSV_FILE) {
            let file = File::open(filename).expect("open file");
            Reader::from_reader(Box::new(file))
        } else {
            Reader::from_reader(Box::new(std::io::stdin()))
        };

    if matches.get_flag(ARG_ID_SHOW_HEADERS) {
        let headers = reader.headers().expect("read headers");

        for (index, header) in (0..headers.len()).zip(headers.into_iter()) {
            if headers.len() > 9 && index < 10 {
                println!(" {index}: {header}")
            } else {
                println!("{index}: {header}");
            }
        }

        return;
    }

    let columns_to_output: Vec<usize> = {
        let header_to_output: Vec<Header> = matches
            .get_many::<String>(ARG_ID_SELECT_HEADER)
            .ok_or("no header provided")
            .expect("header")
            .map(|value| Header::from(value))
            .collect();

        let headers = reader.headers().expect("headers");

        header_to_output
            .iter()
            .map(|header| match header {
                Header::Index(i) => {
                    if *i < headers.len() {
                        *i
                    } else {
                        todo!()
                    }
                }
                Header::Name(name) => {
                    (0..headers.len())
                        .zip(headers.iter())
                        .find(|(_index, value)| *value == *name)
                        .unwrap()
                        .0
                }
            })
            .collect::<Vec<usize>>()
    };

    for record in reader.records() {
        match record {
            Ok(record) => {
                let output = columns_to_output
                    .iter()
                    .map(|i| record.get(*i).expect("header"))
                    .collect::<Vec<&str>>()
                    .join(",");

                println!("{output}");
            }
            Err(err) => eprintln!("error: {err}"),
        }
    }
}
