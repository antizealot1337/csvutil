# CSV Util

A tool to do a litte processing on CSV files. It can output the headers for a
file or the values of a header.

```txt
Usage: csvutil [OPTIONS] [CSV file]

Arguments:
  [CSV file]  The CSV file to parse

Options:
  -L, --show-headers     Print the headers (first line) then exit
  -H, --header <header>  Select the header by index or name for printing
  -h, --help             Print help
  -V, --version          Print version
```
